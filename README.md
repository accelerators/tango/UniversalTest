**The official UniversalTest repository is now available here: https://gitlab.com/tango-controls/device-servers/DeviceClasses/simulation/UniversalTest**


[![logo](http://www.tango-controls.org/static/tango/img/logo_tangocontrols.png)](http://www.tango-controls.org)

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

# UniversalTest

UniversalTest Tango device server

The goal of this device server is to be able to provide attributes of every possible Tango type (DevEncoded type is not 
yet supported).  
Some dynamic attributes in expert display level are created to be able to control the behaviour of the other attributes.  
It is possible to change dynamically the Quality Factor of a given `xxx` attribute via the attributes named `xxx_quality_`.  
It is possible to make `xxx` attribute throw exceptions on demand via the attributes named `xxx_throw_`.  
It is possible to control the time it takes to read `xxx` attribute via the attributes named `xxx_usleep_`.

## Keeping Up-to-date

This repository uses git submodules.

- Ensure that you use `--recurse-submodules` when cloning:

`git clone --recurse-submodules ...`

- If you didn't clone with `--recurse-submodules`, run

  `git submodule update --init`

  to initialise and fetch submodules data.

- Ensure that updates to git submodules are pulled:

`git pull --recurse-submodules`

## Building

To build:

```
mkdir build & cd build
cmake ..
```

By default (on Linux), CMake will use pkg-config to find your installation of
tango.  This can be disabled by passing -DTango_USE_PKG_CONFIG=OFF to CMake. If
pkg-config cannot find tango, or pkg-config is disabled, CMake will try to
search for tango and its dependencies on your system.  You can provide hints to
cmake using either the -DCMAKE_PREFIX_PATH variable or the -DTango_ROOT variable
(cmake 3.12 or later).

### Notable CMake Variables

| Name                       | Default                  | Description                                                     | Notes                            |
|----------------------------|--------------------------|-----------------------------------------------------------------|----------------------------------|
| -DTango_USE_PKG_CONFIG     | OFF (Windows) ON (Linux) | Use pkg-config to find Tango                                    |                                  |
| -DTango_FORCE_STATIC       | OFF                      | Force UniversalTest to link against the static libtango library | Fails if no static library found |
| -DCMAKE_PREFIX_PATH        | ""                       | ;-separated list of prefix paths to search for dependencies in  |                                  |
| -DTango_ROOT               | ""                       | Prefix path to find Tango dependency                            | CMake 3.12 or later              |
| -DZeroMQ_ROOT              | ""                       | Prefix path to find ZeroMQ dependency                           | CMake 3.12 or later              |
| -Dcppzmq_ROOT              | ""                       | Prefix path to find cppzmq dependency                           | CMake 3.12 or later              |
| -DomniORB4_ROOT            | ""                       | Prefix path to find omniORB4 dependency                         | CMake 3.12 or later              |
| -DTANGO_WARNINGS_AS_ERRORS | OFF                      | Treat compiler warnings as errors                               |                                  |

